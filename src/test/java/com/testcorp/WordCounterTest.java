package com.testcorp;

import com.testcorp.translation.Translator;
import com.testcorp.validation.AlphabeticValidator;
import com.testcorp.validation.ArgumentValidator;
import com.testcorp.validation.ValidationException;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.concurrent.ConcurrentHashMap;



@RunWith(MockitoJUnitRunner.class)
public class WordCounterTest {

    @Test(expected = ValidationException.class)
    public void addWord_whenArgumentIsEmpty_then_throwValidationException(){
        String emptyText = "  ";
        wordCounter.addWord(emptyText);
    }

    @Test(expected = ValidationException.class)
    public void addWord_whenArgumentIsNull_then_throwValidationException(){
        String textIsNull = null;
        wordCounter.addWord(textIsNull);
    }

    @Test(expected = ValidationException.class)
    public void addWord_whenArgumentHasIsNotAlphabetic_then_throwValidationException(){
        String textWithDigit = "someTextWithADigit5";
        wordCounter.addWord(textWithDigit);
    }

    @Test()
    public void addWord_whenArgumentIsValid_noException(){
        String text = "someTextWithADigit";
        mockTranslation(text,text);
        wordCounter.addWord(text);
    }


    @Test()
    public void addWord_wordAddedOnceInDifferentLanguage(){
        String wordInFrench = "wordInFrench";
        mockTranslation(wordInFrench,"word");
        wordCounter.addWord(wordInFrench);
        Assert.assertThat(map.get("word"), CoreMatchers.is(1));
    }

    @Test()
    public void addWord_wordAddedTwiceInDifferentLanguage(){
        String wordInFrench = "wordInFrench";
        String wordInSpanish = "wordInSpanish";
        mockTranslation(wordInFrench,"word");
        mockTranslation(wordInSpanish,"word");
        wordCounter.addWord(wordInFrench);
        wordCounter.addWord(wordInSpanish);
        System.out.println(map);
        Assert.assertThat(map.get("word"), CoreMatchers.is(2));
    }



    @Test()
    public void getCountFor_whenWordNotAdded_then_return1(){
        String newWord = "newWord";
        int count = wordCounter.getCountFor(newWord);
        Assert.assertThat(count, CoreMatchers.is(0));
    }
    @Test()
    public void getCountFor_whenWordAdded_then_return0(){
        String newWord = "newWord";
        mockTranslation(newWord,newWord);
        wordCounter.addWord(newWord);
        int count = wordCounter.getCountFor(newWord);
        Assert.assertThat(count, CoreMatchers.is(1));
    }

    private void mockTranslation(String word, String translation) {
        Mockito.when(translator.toEnglish(word)).thenReturn(translation);
    }
    @Before
    public void setUp(){
        map = new ConcurrentHashMap<>();
        wordCounter.setArgumentValidator(validator);
        wordCounter.setWordContainer(map);
    }

    @InjectMocks
    private DefaultWordCounter wordCounter = new DefaultWordCounter();


    private ArgumentValidator validator = new AlphabeticValidator();
    private ConcurrentHashMap<String,Integer> map;
    @Mock
    private Translator translator;
}