package com.testcorp.translation;

public interface Translator {

    /**
     * returns English translation if the word is not in english
     * @param word
     * @return
     */
    String toEnglish(String word);

}
