package com.testcorp.validation;

import javax.xml.stream.events.Characters;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AlphabeticValidator implements ArgumentValidator {
    private String pattern = "[a-zA-Z]+";

    @Override
    public void validate(String word) {
        if(word==null || word.trim().isEmpty())
            throw new ValidationException("Argument is null or empty");
        boolean notAlphanumeric = !word.matches(pattern);
        if(notAlphanumeric)
            throw new ValidationException("Argument contains non-alphabetic characters");
    }
}
