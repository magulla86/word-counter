package com.testcorp.validation;

public interface ArgumentValidator {

    /**Validates the word and throws exception, if invalid
     * @param word
     * @throws ValidationException
     */
    void validate(String word) throws ValidationException;
}
