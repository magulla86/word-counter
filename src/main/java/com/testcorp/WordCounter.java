package com.testcorp;

public interface WordCounter {
    /**
     * Adds word to the container, incrementing the counter
     * @param word
     */
    void addWord(String word);

    /**
     * Get the number of occurrences of the word, including in another language
     * @param word
     */
    int getCountFor(String word);
}
