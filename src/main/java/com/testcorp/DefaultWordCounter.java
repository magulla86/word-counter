package com.testcorp;

import com.testcorp.translation.Translator;
import com.testcorp.validation.ArgumentValidator;


import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;

public class DefaultWordCounter implements WordCounter {

    private ArgumentValidator argumentValidator;
    private Translator translator;
    private ConcurrentHashMap<String,Integer> words;

    @Override
    public void addWord(String word) {
        argumentValidator.validate(word);
        String wordInEglish = translator.toEnglish(word);
        words.compute(wordInEglish, addOrIncrementIfPresent());
    }

    private BiFunction<? super String,? super Integer,? extends Integer> addOrIncrementIfPresent() {
        BiFunction<? super String, ? super Integer, ? extends Integer> function =(key,value)->(value==null?1:(value+1));
        return function;
    }


    @Override
    public int getCountFor(String word) {
        Integer count = words.getOrDefault(word,0);
        return count;

    }

    public void setArgumentValidator(ArgumentValidator argumentValidator) {
        this.argumentValidator = argumentValidator;
    }

    public void setTranslator(Translator translator) {
        this.translator = translator;
    }

    public void setWordContainer(ConcurrentHashMap<String, Integer> words) {
        this.words = words;
    }
}
