package com.testcorp;

import com.testcorp.translation.DefaultTranslator;
import com.testcorp.validation.AlphabeticValidator;

import java.util.concurrent.ConcurrentHashMap;

public class Application {

    public static void main(String[] args) {
        DefaultWordCounter wordCounter = new DefaultWordCounter();
        wordCounter.setArgumentValidator(new AlphabeticValidator());
        wordCounter.setTranslator(new DefaultTranslator());
        wordCounter.setWordContainer(new ConcurrentHashMap<>());
    }
}
